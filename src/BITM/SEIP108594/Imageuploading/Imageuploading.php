<?php

namespace App\BITM\SEIP108594\Imageuploading;

use PDO;

error_reporting(E_ALL ^ E_DEPRECATED);

class Imageuploading {

    public $id = '';
    public $title = '';
    public $user_name = '';
    public $image_name = '';
    public $conn = '';
    public $dbuser = 'root';
    public $pw = '';

    public function __construct() {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=crud_project', $this->dbuser, $this->pw);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//            echo "Successfully connected";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function prepare($data = '') {
//       echo "<pre>";
//       print_r($data);
//       echo "</pre>";
//       die();
        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $this->user_name = $data['name'];
        }
        if (array_key_exists('image', $data) && !empty($data['image'])) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

//    public function test(){
//        echo $this->user_name;
//        echo "<br/>". $this->image_name;
//    }

    public function store() {
        try {
            $stmt = "INSERT INTO `picture` (`name`, `picture`) VALUES (:p, :i)";
            $q = $this->conn->prepare($stmt);
            $q->execute(array(
                ':p' => $this->user_name,
                ':i' => $this->image_name,
            ));

            if ($q) {
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['Message'] = 'Successfully added';
                header('location:create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        header('location:index.php');
    }

    public function index() {
        $mydata = array();
        $stmt = "SELECT * FROM `picture` WHERE `deleted` IS NULL";
        $q = $this->conn->query($stmt) or die('Unable to query');
        while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
            $mydata[] = $row;
        }

        return $mydata;
        header('location:index.php');
    }

    public function show($id = '') {
        $this->id = $id;
//        echo $this->id;
        $stmt = "SELECT * FROM `picture` WHERE  id=" . $this->id;
        $q = $this->conn->query($stmt) or die('Unable to query');

        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function delete($id = '') {
        session_start();
        $this->id = $id;
        $stmt = "DELETE FROM `crud_project`.`picture` WHERE `picture`.`id` =" . $this->id;
        if ($q = $this->conn->query($stmt) or die('Unable to query')) {
            $_SESSION['Message'] = "<h1>" . "Deleted Successfully" . "</h1>";
        } else {
            $_SESSION['Message'] = "<h1>" . "Oops!Something wrong to delete data" . "</h1>";
        }
        header('location:index.php');
    }

    public function trash() {
        session_start();
        $stmt = "UPDATE `crud_project`.`picture` SET `deleted` ='" . date('Y-m-d') . "' WHERE `picture`.`id` =" . $this->id;
        if ($q = $this->conn->query($stmt) or die('Unable to query')) {
            $_SESSION['Message'] = "<h1>" . "Deleted Successfully" . "</h1>";
        } else {
            $_SESSION['Message'] = "<h1>" . "Oops!Something wrong to delete data" . "</h1>";
        }
        header('location:index.php');
    }

    public function trashted() {
        $mydata = array();
        $stmt = "SELECT * FROM `picture` WHERE `deleted` IS NOT NULL ";
        $q = $this->conn->query($stmt) or die('Unable to query');
        while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
            $mydata[] = $row;
        }
        return $mydata;
        header('location:index.php');
    }

    public function update() {
        session_start();
        if (isset($this->image_name) && !empty($this->image_name)) {
            $stmt = "UPDATE `crud_project`.`picture` SET `name` = '" . $this->user_name . "', `picture` = '" . $this->image_name . "' WHERE `picture`.`id` =" . $this->id;
        } else {
            $stmt = "UPDATE `crud_project`.`picture` SET `name` = '" . $this->user_name . "' WHERE `picture`.`id` =" . $this->id;
        }
//        echo $stmt;
//        die();
//        mysql_query($query);
        $q = $this->conn->prepare($stmt);
        $q->execute(array(
            ':p' => $this->user_name,
            ':i' => $this->image_name,
        ));
        $_SESSION['Message'] = "<h2>" . "Data Successfully Updated" . "</h2>";
        header('location:index.php');
    }

    public function updateActive() {
        $sql = "UPDATE `crud_project`.`picture` SET  `active` = 0";
        $q = $this->conn->query($sql) or die('Unable to query');
        $stmt = "UPDATE `crud_project`.`picture` SET  `active` = 1 WHERE `picture`.`id` =" . $this->id;

//        echo $query;
//        die();
        $p = $this->conn->query($stmt) or die('Unable to query');
        $_SESSION['Message'] = "<h2>" . "Profile Picture Activated" . "</h2>";
        header('location:index.php');
    }

    public function active() {
        $stmt = "SELECT * FROM `picture` WHERE `active`=1";
        $q = $this->conn->query($stmt) or die('Unable to query');
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

}
