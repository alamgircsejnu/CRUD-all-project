<?php
namespace App\BITM\SEIP108594\Picture;

class Picture {
   
    public $id = '';
    public  $name = '';
    public $picture = '';
    
    public function __construct() {
        $conn = mysql_connect('localhost','root','') or die('Cannot Connect');
        mysql_select_db('crud_project');
    }
    
    public function prepare($data = '') {
        if (array_key_exists('name',$data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('picture',$data)) {
            $this->picture = $data['picture'];
        }
        if (array_key_exists('id',$data)) {
            $this->id = $data['id'];
        }
    }
    
    public function store() {
        $query = "INSERT INTO `crud_project`.`picture` (`id`, `name`, `picture`, `created`, `updated`, `deleted`) VALUES (NULL, '".$this->name."', '".$this->picture."', '".  date('Y-m-d')."', NULL, NULL)";
        if (mysql_query($query)) {
            session_start();
            $_SESSION['Message'] = "Profile Picture Uploaded";
            header('location:index.php');
        }
    }
    
      public function index() {
        $mydata = array();
        $query = "SELECT * FROM `picture`";
        $result = mysql_query($query);
//        $row = mysql_fetch_assoc($result);
//        return $row;
        while ($row = mysql_fetch_assoc($result)) {
            $mydata[] = $row;
        }
        return $mydata;
        header('location:index.php');
    }
    
     public function show($id = '') {
        $this->id = $id;
        echo $this->id;
        $query = "SELECT * FROM `picture` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
   public function update() {
        session_start();
        if (isset($this->picture) && !empty($this->picture)) {
            $query = "UPDATE `crud_project`.`picture` SET `name` = '" . $this->name . "', `picture` = '" . $this->picture . "' WHERE `picture`.`id` =" . $this->id;
        } else {
            $query = "UPDATE `crud_project`.`picture` SET `name` = '" . $this->name . "' WHERE `picture`.`id` =" . $this->id;
        }
//        echo $query;
//        die();
        mysql_query($query);
        $_SESSION['Message'] = "<h2>" . "Data Successfully Updated" . "</h2>";
        header('location:index.php');
   }
   public function delete($id = '') {
        session_start();
        $this->id = $id;
        $query = "DELETE FROM `crud_project`.`picture` WHERE `picture`.`id` =" . $this->id;
        if(mysql_query($query)){
        $_SESSION['Message'] = "<h1>" . "Deleted Successfully" . "</h1>";
        }  else {
            $_SESSION['Message'] = "<h1>" . "Oops!Something wrong to delete data" . "</h1>";
        }
        header('location:index.php');
    }

    public function trash() {
        session_start();
        $query = "UPDATE `crud_project`.`picture` SET `deleted` ='" . date('Y-m-d') . "' WHERE `picture`.`id` =" . $this->id;
        if(mysql_query($query)){
        $_SESSION['Message'] = "<h1>" . "Deleted Successfully" . "</h1>";
        }  else {
            $_SESSION['Message'] = "<h1>" . "Oops!Something wrong to delete data" . "</h1>";
        }
        header('location:index.php');
    }

    public function trashted() {
        $mydata = array();
        $query = "SELECT * FROM `picture` WHERE `deleted` IS NOT NULL ";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $mydata[] = $row;
        }
        return $mydata;
        header('location:index.php');
    }
   
      public function updateActive() {
        $sql = "UPDATE `crud_project`.`picture` SET  `active` = 0" ;
        mysql_query($sql);
            $query = "UPDATE `crud_project`.`picture` SET  `active` = 1 WHERE `picture`.`id` =" . $this->id;
       
//        echo $query;
//        die();
        mysql_query($query);
        $_SESSION['Message'] = "<h2>" . "Profile Picture Activated" . "</h2>";
        header('location:index.php');
   }
   
   public function active() {
        $query = "SELECT * FROM `picture` WHERE `active`=1";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }
}
