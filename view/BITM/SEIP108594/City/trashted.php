<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\City\City;

if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$City = new City();
$allCities = $City->trashted();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="index.php">See All Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table align="center" border="1">
                <tr>
                    <th>SL</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allCities) && !empty($allCities)) {
                    $serial = 0;
                    foreach ($allCities as $oneCity) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneCity['city']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $oneCity['id'] ?>">Show Details</a>  |
                                <a href="edit.php?id=<?php echo $oneCity['id'] ?>">Edit</a>  |
                                <a href="delete.php?id=<?php echo $oneCity['id'] ?>">Delete Permanently</a>  |
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo"Not available"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            </table>
        </div>
    </body>
</html>