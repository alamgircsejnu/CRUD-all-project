<?php
//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\City\City;

$id = $_GET['id'];
$City = new City();
$oneCity = $City->show($id);
?>
<html>
    <a href="index.php">See All Data</a>
    <div  id="create" align="center">
        <fieldset>
            <legend>Crud Of City</legend>
            <form action="update.php" method="post">
                <br/><br/><br/>
                <label>Select Your City</label>
                <select name="city">
                    <option value=""><?php echo $oneCity['city']; ?></option>
                    <option value="Dhaka">Dhaka</option>
                    <option value="Rajshahi">Rajshahi</option>
                    <option value="Sylhet">Sylhet</option>
                    <option value="Chittagong">Chittagong</option>
                    <option value="Barisal">Barisal</option>
                    <option value="Rangpur">Rangpur</option>
                    <option value="Mymensingh">Mymensingh</option>
                </select>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <input type="submit" value="Update" >
            </form>
        </fieldset>
    </div>
</html>