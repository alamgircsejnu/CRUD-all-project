<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP108594\City\City;
$id=$_GET['id'];
//echo $id;

$City=new City();
$oneMobile=$City->show($id);
?>
<html>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    <a href="index.php">Back</a>
    <table align="center" border="1">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $oneMobile['id'];?></td>
            <td>
                <?php
                if(isset($oneMobile['city']) && !empty($oneMobile['city'])){
                    echo $oneMobile['city'];
                }  else {
                    echo "Data Not Available";
                }
                ?>
            </td>
            <td><?php echo $oneMobile['created'];?></td>
            <td>
                <a href="edit.php?id=<?php echo $oneMobile['id'];?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $oneMobile['id'];?>">Delete</a>
            </td>
        </tr>
    </table>
</html>