<?php
//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Textarea\Textarea;

$id = $_GET['id'];
$textarea = new Textarea();
$onetextarea = $textarea->show($id);
?>
<html>
    <a href="index.php">See All Data</a>
    <div  id="create" align="center">
        <fieldset>
            <legend>Update | Summary of Organization</legend>
            <form action="update.php" method="post">
                <br/>
                <label>Name Of Organization</label>
                <textarea name="title" cols="40" rows="10" ><?php echo $onetextarea['title']; ?></textarea>
                <input type="submit" value="Update" name="update">
                <input type="hidden" name="id" value="<?php echo $id; ?>"
            </form>
        </fieldset>
    </div>
</html>