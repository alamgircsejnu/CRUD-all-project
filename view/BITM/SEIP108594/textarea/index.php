<?php
//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Textarea\Textarea;

$obj = new Textarea();
$alltextarea = $obj->index();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1" align="center">
                <tr>
                    <th>SL</th>
                    <th>Organization Name</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($alltextarea) && !empty($alltextarea)) {
                    $serial = 0;
                    foreach ($alltextarea as $onetextarea) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $onetextarea['title']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $onetextarea['id']; ?>">Show Details</a> | 
                                <a href="edit.php?id=<?php echo $onetextarea['id']; ?>">Edit</a> | 
                                <a href="trash.php?id=<?php echo $onetextarea['id']; ?>">Delete</a> | 

                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
                    <?php echo "Not Available Data" ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </body>
</html>
