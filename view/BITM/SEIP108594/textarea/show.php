<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP108594\Textarea\Textarea;

$id=$_GET['id'];
$show = new Textarea();
$onetextarea=$show->show($id);

?>
<html>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    <a href="index.php">Back</a>
    <table align="center" border="1">
        <tr>
            <th>ID</th>
            <th>Organization Name</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onetextarea['id'];?></td>
            <td>
                <?php 
                if(isset($onetextarea) && !empty($onetextarea)){
                    echo $onetextarea['title'];
                }else{
                    echo "Not Available";
                }?>
            </td>
            <td><?php echo $onetextarea['created'];?></td>
            <td>
                <a href="edit.php?id=<?php echo $onetextarea['id'];?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $onetextarea['id'];?>">Delete</a> | 
            </td>
        </tr>
    </table>
</html>
