<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Radio\Radio;

//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$store = new Radio();
$allGender = $store->index();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table align="center" border="1">
                <tr>
                    <th>SL</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allGender) && !empty($allGender)) {
                    $serial = 0;
                    foreach ($allGender as $oneGender) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneGender['gender']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $oneGender['id'] ?>">Show Details</a> | 
                                <a href="edit.php?id=<?php echo $oneGender['id'] ?>">Edit</a> | 
                                <a href="trash.php?id=<?php echo $oneGender['id'] ?>">Delete</a> | 
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo "Not Available" ?>

                        </td>
                    </tr>
<?php }
?>

            </table>
        </div>
    </body>
</html>

