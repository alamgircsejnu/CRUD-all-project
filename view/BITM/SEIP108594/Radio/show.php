<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP108594\Radio\Radio;
$id=$_GET['id'];
//echo $id;

$Radio=new Radio();
$oneGender=$Radio->show($id);
?>
<html>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    <a href="index.php">Back</a>
    <table align="center" border="1">
        <tr>
            <th>ID</th>
            <th>Gender</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $oneGender['id'];?></td>
            <td>
                <?php
                if(isset($oneGender['gender']) && !empty($oneGender['gender'])){
                    echo $oneGender['gender'];
                }  else {
                    echo "Data Not Available";
                }
                ?>
            </td>
            <td><?php echo $oneGender['created'];?></td>
            <td>
                <a href="edit.php?id=<?php echo $oneGender['id'];?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $oneGender['id'];?>">Delete</a>
            </td>
        </tr>
    </table>
</html>