<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Radio\Radio;

if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$Radio = new Radio();
$allRadio = $Radio->trashted();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="index.php">See All Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table align="center" border="1">
                <tr>
                    <th>SL</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allRadio) && !empty($allRadio)) {
                    $serial = 0;
                    foreach ($allRadio as $oneRadio) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneRadio['gender']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $oneRadio['id'] ?>">Show Details</a>  |
                                <a href="edit.php?id=<?php echo $oneRadio['id'] ?>">Edit</a>  |
                                <a href="delete.php?id=<?php echo $oneRadio['id'] ?>">Delete Permanently</a>  |
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo"Not available"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            </table>
        </div>
    </body>
</html>