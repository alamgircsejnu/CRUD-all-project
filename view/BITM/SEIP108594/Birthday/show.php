<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP108594\Birthday\Birthday;

$id=$_GET['id'];
//echo $id;
$show = new Birthday();
$onebirthday = $show->show($id);

?>
<html>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    <a href="index.php">Back</a>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Birthday</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onebirthday['id'];?></td>
            <td>
                <?php
                if(isset($onebirthday['birthday']) && !empty($onebirthday['birthday'])){
                    echo $onebirthday['birthday'];
                }else{
                    echo "Not Available Data";
                }
                    
                ?>
            </td>
            <td><?php echo $onebirthday['created'];?></td>
            <td>
                <a href="edit.php?id=<?php echo $id?>">Edit</a> |
                <a href="delete.php?id=<?php echo $id?>">Delete</a>
            </td>
        </tr>
    </table>
</html>