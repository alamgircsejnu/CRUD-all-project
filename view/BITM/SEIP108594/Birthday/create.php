<?php
include_once './navigation.php';
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>

<html>
    <head>
        <title>Create | Birthday</title>
        <!--<link href="../../../../css/style.css" rel="stylesheet" type="text/css">-->
        <link href="../../../../js/css/blitzer/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="../../../../js/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="../../../../js/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script type="text/javascript">
            $(function () {
            $("#input-field").datepicker({
            dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0",
                    showOn: "button",
                    buttonImage: "../../../../picture/cal.png",
                    buttonImageOnly: true,
                    buttonText: "Select date",
                    showOn:"both"
            });
            });
        </script>
        <style>
            .ui-datepicker{
                font-size: 15px;
            }
        </style>
    </head>
    <body>
        <a href="index.php">See All Data</a>
        <div  id="create" align="center">
            <fieldset>
                <legend>
                    Crud of Birthday
                </legend> 
                <form action="store.php" method="POST">
                    <br/><br/><br/>
                    <label>Enter Your Birthday</label>
                    <input type="text" name="birthday" id="input-field" placeholder="YYYY-MM-DD">
                    <!--<img src="../../../../picture/cal.png">-->
                    <br/><br/>
                    <input type="submit" value="Save">
                    <!--<input type="submit" value="Save & Enter Again">-->
                    <input type="reset" value="Reset">
                </form>
            </fieldset>
        </div>
    </body>
</html>