<?php
//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Mobile\Mobile;

$id = $_GET['id'];
$Mobile = new Mobile();
$oneMobile = $Mobile->show($id);
?>
<html>
    <a href="index.php">See All Data</a>
    <div  id="create" align="center">
        <fieldset>
            <legend>Crud Of Mobile Model</legend>
            <form action="update.php" method="post">
                <br/><br/><br/>
                <label>UR Favourite Mobile Model</label>
                <input type="text" name="title" id="input-field" size="40" value="<?php echo $oneMobile['title']; ?>">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <input type="submit" value="Update" >
            </form>
        </fieldset>
    </div>
</html>