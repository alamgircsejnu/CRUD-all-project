<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Hobby\Hobbies;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$hobby = new Hobbies();
$allhobbies = $hobby->index();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1" align="center">
                <tr>
                    <th>SL</th>
                    <th>Hobby</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allhobbies) && !empty($allhobbies)) {
                    $serial = 0;
                    foreach ($allhobbies as $onehobby) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $onehobby['hobby']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $onehobby['id']; ?>">Show Details</a> | 
                                <a href="edit.php?id=<?php echo $onehobby['id']; ?>">Edit</a> | 
                                <a href="trash.php?id=<?php echo $onehobby['id']; ?>">Delete</a>  
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
                            <?php echo "Not Available" ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </body>
</html>