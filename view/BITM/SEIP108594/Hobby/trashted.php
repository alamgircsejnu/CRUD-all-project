<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Hobby\Hobbies;

if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$hobby = new Hobbies();
$allhobbiess = $hobby->trashted();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="index.php">See All Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table align="center" border="1">
                <tr>
                    <th>SL</th>
                    <th>Book Title</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allhobbiess) && !empty($allhobbiess)) {
                    $serial = 0;
                    foreach ($allhobbiess as $onehobby) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $onehobby['hobby']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $onehobby['id'] ?>">Show Details</a>  |
                                <a href="edit.php?id=<?php echo $onehobby['id'] ?>">Edit</a>  |
                                <a href="delete.php?id=<?php echo $onehobby['id'] ?>">Delete Permanently</a>  |
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo"Not available"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            </table>
        </div>
    </body>
</html>