<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP108594\Book\Book;
$id=$_GET['id'];
$Book =new Book();
$onebook=$Book->show($id);
?>
<html>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    <a href="index.php">Back</a>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Book Title</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onebook['id']?></td>
            <td>
                <?php 
                if(isset($onebook['title']) && !empty($onebook['title'])){
                    echo $onebook['title'];
                }else{
                    echo "Not Available";
                }
                ?>
            </td>
            <td><?php echo $onebook['created']?></td>
            <td>
                <a href="edit.php?id=<?php echo $onebook['id'];?>">Edit</a> | 
                <a href="delete.php?id=<?php echo $onebook['id'];?>">Delete</a> 
            </td>
        </tr>
    </table>
</html>
