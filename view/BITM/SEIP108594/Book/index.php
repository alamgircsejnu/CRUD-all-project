<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Book\Book;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$Books = new Book();
$allbooks = $Books->index();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1" align="center">
                <tr>
                    <th>SL</th>
                    <th>Book Title</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allbooks) && !empty($allbooks)) {
                    $serial = 0;
                    foreach ($allbooks as $onebook) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $onebook['title']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $onebook['id']; ?>">Show Details</a> | 
                                <a href="edit.php?id=<?php echo $onebook['id']; ?>">Edit</a> | 
                                <a href="trash.php?id=<?php echo $onebook['id']; ?>">Delete</a>  
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
                            <?php echo "Not Available" ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </body>
</html>
