<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Book\Book;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$id = $_GET['id'];
$Book = new Book();
$onebook = $Book->show($id);
?>
<html>
    <a href="index.php">See All Data</a>
    <div  id="create" align="center">
        <fieldset>
            <legend>Book Title Form</legend>
                <form action="update.php" method="post">
                    <br/><br/><br/>
                    <label>Book Title</label>
                    <input type="text" name="title" id="input-field" value="<?php echo $onebook['title']; ?>">
                    <input type="submit" name="update" value="update">  
                        <input type="hidden" name="id"value="<?php echo $id; ?>">
                </form>
        </fieldset>
    </div>
</html>
