<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Picture\Picture;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$picture = new Picture();
$allPictures = $picture->index();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
                <li><a href="activated.php">Activated Profile Picture</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1">
                <tr>
                    <th>
                        SL
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Profile Picture
                    </th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allPictures) && !empty($allPictures)) {
                    $serial = 0;
                    foreach ($allPictures as $onePicture) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $onePicture['name'] ?></td>                 
                            <td><img src="<?php echo "../../../../img/" . $onePicture['picture'] ?>" width="200" height="140"></td>                 
                            <td>
                                <a href="show.php?id=<?php echo $onePicture['id'] ?>">Show Details</a>  |
                                <a href="edit.php?id=<?php echo $onePicture['id'] ?>">Edit</a>  |
                                <a href="trash.php?id=<?php echo $onePicture['id'] ?>">Delete</a>  |
                                <a href="active.php?id=<?php echo $onePicture['id'] ?>">Active</a> 


                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
                            <?php echo "No avilable Data"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </body>
</html>


