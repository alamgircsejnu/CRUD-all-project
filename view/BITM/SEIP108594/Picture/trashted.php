
<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once './navigation.php';
include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP108594\Picture\Picture;

//use App\BITM\SEIP108594\Utility\Utility;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$Picture = new Picture();

$AllPicture = $Picture->trashted();

//echo "<pre>";
//print_r($AllPicture);
//$dbg = new Utility();
//$dbg->debug($AllPicture);
//die();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="index.php">See All Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1">
                <tr>
                    <th>
                        SL
                    </th>
                    <th>
                        User Name
                    </th>
                    <th>
                        Profile Pic
                    </th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($AllPicture) && !empty($AllPicture)) {
                    $serial = 0;
                    foreach ($AllPicture as $OnePicture) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $OnePicture['name'] ?></td>                 
                            <td><img src="<?php echo "../../../../img/" . $OnePicture['picture']; ?>" width="200" height="140"></td>                 
                            <td>
                                <a href="show.php?id=<?php echo $OnePicture['id'] ?>">Show Details</a>  |
                                <a href="edit.php?id=<?php echo $OnePicture['id'] ?>">Edit</a>  |
                                <a href="delete.php?id=<?php echo $OnePicture['id'] ?>">Delete</a> 
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
                            <?php echo "No avilable Data"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </body>
</html>