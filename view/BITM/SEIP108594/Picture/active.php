<?php
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Picture\Picture;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$picture = new Picture();
$picture ->prepare($_GET);
$picture ->updateActive();
