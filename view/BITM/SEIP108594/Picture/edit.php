<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once './navigation.php';
include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP108594\Picture\Picture;

//use App\BITM\SEIP50\Utility\Utility;
//echo $_GET['id'];
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$Picture = new Picture();

$one = $Picture->show($_GET['id']);

//$dbg = new Utility();
//$dbg->debug($one);
//die();
?>
<html>
    <a href="index.php">See All Data</a>
    <div  id="create" align="center">
        <fieldset>
            <legend>
                Update Profile Information
            </legend> 
            <form action="update.php" method="post" enctype="multipart/form-data">
                <label>Name:</label>
                <input type="text" name="name" value="<?php echo $one['name'] ?>">


                <input type="file" name="image">
                <img src="<?php echo "../../../../img/" . $one['picture'] ?>"/>
                <input type="submit" value="Update">

                <input type="reset" value="Reset">
                <input type="hidden" value="<?php echo $_GET['id'] ?>" name="id">
            </form>
        </fieldset>
    </div>
</html>