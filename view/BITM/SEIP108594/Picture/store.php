<?php
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Picture\Picture;

$picture = new Picture();

//$picture = $_FILES['picture'];
//var_dump($picture);

if (isset($_FILES['picture'])) {
    $errors = array();
    $pictureName = time().$_FILES['picture']['name'];
    $pictureType = $_FILES['picture']['type'];
    $pictureTempName = $_FILES['picture']['tmp_name'];
    $pictureSize = $_FILES['picture']['size'];
    $explodedName = explode('.', $pictureName);
    $fileExtension = strtolower(end($explodedName));
    
    $format = array('jpeg','jpg','png');
    if (in_array($fileExtension, $format) === FALSE){
        $errors = "Wrong Format";
    }
    if (empty($errors)===TRUE) {
        move_uploaded_file($pictureTempName, "../../../../img/".$pictureName);
        $_POST['picture'] = $pictureName;
    }
}

$picture->prepare($_POST);
$picture->store();


