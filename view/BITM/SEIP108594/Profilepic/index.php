
<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once './navigation.php';
include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP108594\Imageuploading\Imageuploading;

//use App\BITM\SEIP108594\Utility\Utility;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$Picture = new Imageuploading();

$AllPicture = $Picture->index();

//echo "<pre>";
//print_r($AllPicture);
//$dbg = new Utility();
//$dbg->debug($AllPicture);
//die();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
                <li><a href="activated.php">Activated Profile Picture</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1">
                <tr>
                    <th>
                        SL
                    </th>
                    <th>
                        User Name
                    </th>
                    <th>
                        Profile Pic
                    </th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($AllPicture) && !empty($AllPicture)) {
                    $serial = 0;
                    foreach ($AllPicture as $OnePicture) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $OnePicture['name'] ?></td>                 
                            <td><img src="<?php echo "../../../../img/" . $OnePicture['picture']; ?>" width="200" height="140"></td>                 
                            <td>
                                <a href="show.php?id=<?php echo $OnePicture['id'] ?>">Show Details</a>  |
                                <a href="edit.php?id=<?php echo $OnePicture['id'] ?>">Edit</a>  |
                                <a href="trash.php?id=<?php echo $OnePicture['id'] ?>">Delete</a>  | 
                                <a href="active.php?id=<?php echo $OnePicture['id'] ?>">Active</a>  |
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
                            <?php echo "No avilable Data"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </body>
</html>