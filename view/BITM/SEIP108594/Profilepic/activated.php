<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Imageuploading\Imageuploading;

$picture = new Imageuploading();
$onePicture = $picture->active();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
                <li><a href="activated.php">Activated Profile Picture</a></li>
            </ul>
        </div>
        <br>
        <div id="activated-picture">
            <br/><br/><br/><br/><br/>
            <img src="<?php echo "../../../../img/" . $onePicture['picture'] ?>" width="200" height="140">
        </div>
    </body>
</html>

