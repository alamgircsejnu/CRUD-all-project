<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Imageuploading\Imageuploading;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$id = $_GET['id'];
$picture = new Imageuploading();
$onePictures = $picture->show($id);
//print_r($onePictures);
//die();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table border="1">
                <tr>
                    <th>ID</th>

                    <th>Name</th>
                    <th>Profile Picture</th>
                    <th>Created</th>
                    <th>Modified</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td><?php echo $onePictures['id'] ?></td>
                    <td><?php
if (isset($onePictures['name']) && !empty($onePictures['name'])) {
    echo $onePictures['name'];
} else {
    echo "No data available";
}
?>

                    </td>
                    <td><img src="<?php echo "../../../../img/" . $onePictures['picture'] ?>" width="200" height="140"></td>   
                    <td><?php echo $onePictures['created'] ?></td>
                    <td><?php echo $onePictures['updated'] ?></td>
                    <td>
                        <a href="edit.php?id=<?php echo $id; ?>">Edit</a> |
                        <a href="delete.php?id=<?php echo $id; ?>">Delete</a>  |
                        <a href="active.php?id=<?php echo $id ?>">Active</a> 

                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
