<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Email\Email;

//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$Email = new Email();
$allEmail = $Email->index();
?>
<html>
    <head>
        <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <br><br>
        <div id="submenu">
            <ul>
                <li><a href="create.php">Create</a></li>
                <li><a href="trashted.php">See Deleted Data</a></li>
            </ul>
        </div>
        <br>
        <div id="table">
            <table align="center" border="1">
                <tr>
                    <th>SL</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allEmail) && !empty($allEmail)) {
                    $serial = 0;
                    foreach ($allEmail as $oneEmail) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneEmail['email']; ?></td>
                            <td>
                                <a href="show.php?id=<?php echo $oneEmail['id'] ?>">Show Details</a> | 
                                <a href="edit.php?id=<?php echo $oneEmail['id'] ?>">Edit</a> | 
                                <a href="trash.php?id=<?php echo $oneEmail['id'] ?>">Delete</a> | 
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo "Not Available" ?>

                        </td>
                    </tr>
<?php }
?>

            </table>
        </div>
    </body>
</html>

