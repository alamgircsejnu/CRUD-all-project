<?php
include_once './navigation.php';
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP108594\Email\Email;

$id = $_GET['id'];
//echo $id;

$Email = new Email();
$oneEmail = $Email->show($id);
?>
<html>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css">
    <a href="index.php">Back</a>
    <table align="center" border="1">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $oneEmail['id']; ?></td>
            <td>
                <?php
                if (isset($oneEmail['email']) && !empty($oneEmail['email'])) {
                    echo $oneEmail['email'];
                } else {
                    echo "Data Not Available";
                }
                ?>
            </td>
            <td><?php echo $oneEmail['created']; ?></td>
            <td>
                <a href="edit.php?id=<?php echo $oneEmail['id']; ?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $oneEmail['id']; ?>">Delete</a>
            </td>
        </tr>
    </table>
</html>