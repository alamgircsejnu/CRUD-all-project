-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2016 at 08:40 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `birthday`, `created`, `updated`, `deleted`) VALUES
(2, '2016-03-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(3, '2016-03-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(4, '1994-11-25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(0, '1991-01-18', '2016-04-05 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(0, '1991-01-18', '2016-04-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(15) NOT NULL,
  `title` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'MySQL Database', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Object Oriented Programming', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-22'),
(7, 'C++', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'OOP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 'PHP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 'Database', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `created`, `updated`, `deleted`) VALUES
(1, 'Dhaka', NULL, NULL, NULL),
(2, 'Dhaka', '2016-04-05', NULL, '2016-04-05'),
(3, 'Chittagong', '2016-04-05', NULL, NULL),
(4, 'Mymensingh', '2016-04-05', NULL, NULL),
(5, 'Rangpur', '2016-04-05', NULL, NULL),
(6, 'Sylhet', '2016-04-05', NULL, '2016-04-05'),
(7, 'Rajshahi', '2016-04-05', NULL, '2016-04-05'),
(8, 'Mymensingh', '2016-04-06', NULL, '2016-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `created`, `updated`, `deleted`) VALUES
(3, 'alamgircsejnu@gmail.com', NULL, NULL, NULL),
(4, 'shahabuddinp91@gmaill.com', NULL, NULL, NULL),
(7, 'priyanka@gmail.com', '2016-03-29', NULL, '2016-04-06'),
(8, 'jannatul@gmail.com', '2016-03-29', NULL, '2016-04-06'),
(9, 'Ferdaus@gmail.com', '2016-03-29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(15) NOT NULL,
  `hobby` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobby`, `created`, `updated`, `deleted`) VALUES
(1, 'Cricket , Codding', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Cricket,Football,Codding,Gardening', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-06'),
(4, 'Cricket , Gardening', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'Cricket , Football , Codding', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby2`
--

CREATE TABLE `hobby2` (
  `id` int(15) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby2`
--

INSERT INTO `hobby2` (`id`, `hobby`, `created`, `updated`, `deleted`) VALUES
(1, 'Cricket', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` int(15) NOT NULL,
  `title` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Nokia 1200', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-24'),
(2, 'Nokia 2690', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Walton Primo F4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-24'),
(4, 'Walton Primo E6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-24'),
(5, 'Symphoni x123', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'Web Development', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-25'),
(7, 'Walton Primo X2 Mini', '2016-04-05 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'test', '2016-04-05 00:00:00', '0000-00-00 00:00:00', '2016-04-05'),
(9, 'Samsung Galaxy S6', '2016-04-06 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`id`, `name`, `picture`, `created`, `updated`, `deleted`, `active`) VALUES
(5, 'Shahab Uddin', '1459659026download (1).jpg', '2016-04-03', NULL, NULL, 0),
(8, 'Azad Naim', '1459666143images.jpg', '2016-04-03', NULL, NULL, 0),
(11, 'Julfar Nayem', '1459961076Screenshot (22).png', NULL, NULL, '2016-04-06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `radio`
--

CREATE TABLE `radio` (
  `id` int(11) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `update` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `radio`
--

INSERT INTO `radio` (`id`, `gender`, `created`, `update`, `deleted`) VALUES
(6, 'Female', '2016-03-29', NULL, NULL),
(7, 'Male', '2016-03-29', NULL, NULL),
(8, 'Female', '2016-03-29', NULL, NULL),
(9, 'Male', '2016-03-29', NULL, NULL),
(10, 'Female', '2016-03-29', NULL, NULL),
(11, 'Female', '2016-03-29', NULL, NULL),
(12, 'Male', '2016-03-29', NULL, '2016-04-06'),
(13, 'Female', '2016-03-29', NULL, '2016-04-06'),
(14, 'Male', '2016-03-29', NULL, '2016-04-06'),
(15, 'Female', '2016-04-06', NULL, '2016-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `textarea`
--

CREATE TABLE `textarea` (
  `id` int(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `textarea`
--

INSERT INTO `textarea` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(5, 'Hello world!', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'BITM', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 'Jagannath University,Dhaka.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'Basis Institute of Technology and management.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 'test1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-05'),
(10, 'test2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-05'),
(11, 'dfhghjdgghjgyytryghjhgj', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby2`
--
ALTER TABLE `hobby2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radio`
--
ALTER TABLE `radio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `textarea`
--
ALTER TABLE `textarea`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobby2`
--
ALTER TABLE `hobby2`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `radio`
--
ALTER TABLE `radio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `textarea`
--
ALTER TABLE `textarea`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
